import React from 'react';
import { shallow } from 'enzyme';
import Header from './header';
import items from '../server/items';

describe('header', () => {
  describe('when items are loading', () => {
    it('renders the loading message', () => {
      const wrapper = shallow(<Header.WrappedComponent loading={true} items={[]} />);
  
      expect(wrapper.containsMatchingElement(
        <div className="menu-summary">Loading</div>
      )).toEqual(true);
    });
  });

  describe('when there is 1 item loaded', () => {
    it('displays the header with correct counts', () => {
      const dietCounts={"v":1}
      const wrapper = shallow(<Header.WrappedComponent items={[items[0]]} loading={false} dietCounts={dietCounts}/>);

      expect(wrapper.containsMatchingElement(
        <div className="menu-summary">
          <div className="container">
            <div className="row">
              <div className="col-6 menu-summary-left">
              <span>1 items</span>
              </div>
              <div className="col-6 menu-summary-right">
                <React.Fragment key="v">
                  1x <span className="dietary">v</span>
                </React.Fragment>
              </div>
            </div>
          </div>
        </div>
      )).toEqual(true);
    });
  });
});