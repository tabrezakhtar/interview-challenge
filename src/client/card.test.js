import React from 'react';
import { shallow } from 'enzyme';
import Card from './card';
import * as previewActions from './actions/preview-actions';
import items from '../server/items';

describe('sidebar card', () => {
  describe('when an item is passed to the card', () => {
    it('renders the card with the item', () => {
      const wrapper = shallow(<Card.WrappedComponent item={items[0]} position={0}/>);
  
      expect(wrapper.containsMatchingElement(
        <li className="item">
          <h2>Kale Caesar Pasta, Turmeric Satay Broccoli & Lemon Cashew Greens</h2>
          <p>
            <span key="v" className="dietary">v</span>
            <span key="ve" className="dietary">ve</span>
            <span key="df" className="dietary">df</span>
            <span key="gf" className="dietary">gf</span>
            <span key="n!" className="dietary">n!</span>
          </p>
        </li>
      )).toEqual(true);
    });
  });

  describe('when an item is clicked on', () => {
    it('adds the item to the preview list', () => {
      const dispatchStub = jest.fn();
      const addItemSpy = jest.spyOn(previewActions, 'addItem');
      const wrapper = shallow(<Card.WrappedComponent dispatch={dispatchStub} item={items[0]} position={0}/>);
  
      wrapper.find('li').simulate('click');
      expect(addItemSpy).toBeCalledWith(items[0]);
    });
  });

});

describe('preview card', () => {
  describe('when an item is passed to the card as a preview', () => {
    it('renders the card with the item', () => {
      const wrapper = shallow(<Card.WrappedComponent item={items[0]} position={0} isPreview={true}/>);
  
      expect(wrapper.containsMatchingElement(
        <li className="item">
          <h2>Kale Caesar Pasta, Turmeric Satay Broccoli & Lemon Cashew Greens</h2>
          <p>
            <span key="v" className="dietary">v</span>
            <span key="ve" className="dietary">ve</span>
            <span key="df" className="dietary">df</span>
            <span key="gf" className="dietary">gf</span>
            <span key="n!" className="dietary">n!</span>
          </p>

          <button className="remove-item">x</button>
        </li>
      )).toEqual(true);
    });
  });

  describe('when an item is removed', () => {
    it('removes the item from preview list', () => {
      const dispatchStub = jest.fn();
      const removeItemSpy = jest.spyOn(previewActions, 'removeItem');
      const wrapper = shallow(<Card.WrappedComponent dispatch={dispatchStub} item={items[0]} position={0} isPreview={true}/>);
  
      wrapper.find('.remove-item').simulate('click');
      expect(removeItemSpy).toBeCalledWith(0);
    });
  });
});