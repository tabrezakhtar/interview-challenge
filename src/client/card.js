import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {addItem, removeItem} from './actions/preview-actions';

const Card = ({dispatch, position, item, isPreview}) => {
  const handleAddClick = () => {
    dispatch(addItem(item));
  }
  
  const handleRemoveClick = () => {
    dispatch(removeItem(position));
  }

  const closeButton = isPreview ? (
    <button className="remove-item" onClick={() => handleRemoveClick(item)}>
      x
    </button>
  ) : null;

  const dietIcons = item.dietaries ? item.dietaries.map(diet => <span key={diet} className="dietary">{diet}</span>): null;
  return (
    <li className="item" onClick={!isPreview ? () => handleAddClick(item): undefined}>
      <h2>{item.name}</h2>
      <p>
        {dietIcons}
      </p>

      {closeButton}
    </li>
  );
};

Card.propTypes = {
  position: PropTypes.number,
  item: PropTypes.object.isRequired,
  isPreview: PropTypes.bool
};

export default connect()(Card)
