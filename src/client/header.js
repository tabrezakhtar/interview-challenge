import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { flattenDiets } from './items-helper';

const Header = ({items, dietCounts, loading}) => {
  if (loading) {
    return <div className="menu-summary">Loading</div>
  }

  const flattenedDiets = flattenDiets(items);

  const itemMessage = `${items.length} items`;

  const dietCountsList = Object.keys(dietCounts).length ? Object.getOwnPropertyNames(dietCounts).map((key) => (
    <React.Fragment key={key}>
      {dietCounts[key]}x <span className="dietary">{key}</span>
    </React.Fragment>
  )) : flattenedDiets.map((key) => <React.Fragment key={key}>0x <span className="dietary">{key}</span></React.Fragment>)

  return <div className="menu-summary">
      <div className="container">
        <div className="row">
          <div className="col-6 menu-summary-left">
          <span>{itemMessage}</span>
          </div>
          <div className="col-6 menu-summary-right">
            {dietCountsList}
          </div>
        </div>
      </div>
    </div>
}

Header.propTypes = {
  items: PropTypes.array,
  dietCounts: PropTypes.object,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  items: state.items.items,
  dietCounts: state.preview.dietCounts,
  loading: state.items.loading
});

export default connect(mapStateToProps)(Header)