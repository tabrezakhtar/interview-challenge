import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Card from './card';

const Preview = ({items}) => {
  const previewItemsList = items.map((item, index) => (
    <Card
      key={item.id + index.toString()}
      position={index}
      item={item}
      isPreview={true}
    />
  ));

  return (
    <div className="col-8">
      <h2>Menu preview</h2>
      <ul className="menu-preview">
        {previewItemsList}
      </ul>
    </div>
  );
};

Preview.propTypes = {
  items: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  items: state.preview.items
});

export default connect(mapStateToProps)(Preview)