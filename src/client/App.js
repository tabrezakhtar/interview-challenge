import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {fetchItems} from './actions/items-actions'
import Header from './header';
import Sidebar from './sidebar';
import Preview from './preview';
import './App.css';

const App = ({dispatch}) => {

  useEffect(() => {
    dispatch(fetchItems());
  });

  return <div className="wrapper">
    <Header />
    <div className="container menu-builder">
      <div className="row">
        <Sidebar />
        <Preview />
      </div>
    </div>
  </div>
};

export default connect()(App)