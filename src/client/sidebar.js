import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Card from './card';

const Sidebar = ({items, loading}) => {
  if (loading) {
    return (
      <div className="col-4">
        Loading
      </div>
    )
  }

  const itemsList = items.map((item) => (
    <Card
      key={item.id}
      item={item}
    />
  ));

  return (
    <div className="col-4">
      <div className="filters">
        <input className="form-control" placeholder="Name" />
      </div>

      <ul className="item-picker">
        {itemsList}
      </ul>
    </div>
  );
};

Sidebar.propTypes = {
  items: PropTypes.array,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  items: state.items.items,
  loading: state.items.loading
});

export default connect(mapStateToProps)(Sidebar)