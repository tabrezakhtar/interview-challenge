import React from 'react';
import { shallow } from 'enzyme';
import App from './app';
import Header from './header';
import Sidebar from './sidebar';
import Preview from './preview';

describe('when the component loads', () => {
  it('renders the app', () => {
    const wrapper = shallow(<App.WrappedComponent />);

    expect(wrapper.containsMatchingElement(
      <div className="wrapper">
        <Header />
        <div className="container menu-builder">
          <div className="row">
            <Sidebar />
            <Preview />
          </div>
        </div>
      </div>
    )).toEqual(true);
  });
});