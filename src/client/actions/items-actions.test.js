import * as actions from './items-actions';

beforeEach(() => {
  fetch.resetMocks();
});

describe('when item data is fetched', () => {
  test('it dispatches the getItems action', async(done) => {
    const dispatch = jest.fn();
    await actions.fetchItems()(dispatch);
    expect(dispatch).toHaveBeenCalledWith(actions.getItems());
    done();
  });

  test('it dispatches the success action', async(done) => {
    fetch.mockResponseOnce(JSON.stringify({ items: 'mock response' }));
    const dispatch = jest.fn();
    await actions.fetchItems()(dispatch);
    expect(dispatch).toHaveBeenLastCalledWith(actions.getItemsSuccess('mock response'));
    done();
  });
});