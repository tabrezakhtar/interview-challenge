export const GET_ITEMS = 'GET_ITEMS';
export const GET_ITEMS_SUCCESS = 'GET_ITEMS_SUCCESS';
export const GET_ITEMS_FAILURE = 'GET_ITEMS_FAILURE';

export const getItems = () => ({ type: GET_ITEMS });
export const getItemsSuccess = items => ({
  type: GET_ITEMS_SUCCESS,
  payload: items
});
export const getItemsFailure = () => ({ type: GET_ITEMS_FAILURE });

export function fetchItems() {
  return async dispatch => {
    dispatch(getItems());
    try {
      const response = await fetch('/api/items');
      const {items} = await response.json();
      dispatch(getItemsSuccess(items));
    } catch (error) {
      dispatch(getItemsFailure());
    }
  }
}
