import previewReducer from './preview-reducer';
import * as actions from '../actions/preview-actions';
import items from '../../server/items';

describe('preview reducer', () => {
  it('should return the initial state', () => {
    expect(previewReducer(undefined, {})).toEqual({hasErrors: false, items: [], dietCounts: null});
  });

  describe('adding items', () => {
    describe('when 1 item is added', () => {
      it('should have 1 item in the state with the correct diet count', () => {
        expect(previewReducer(undefined, actions.addItem(items[0]))).toEqual({
          hasErrors: false,
          items: [items[0]],
          dietCounts: { "df":1, "gf":1, "n!":1, "v":1, "ve":1}
        });
      });
    });

    describe('when 2 items are added', () => {
      it('should have 2 items in the state with the correct diet count', () => {
        let state = previewReducer(undefined, actions.addItem(items[0]));
        state = previewReducer(state, actions.addItem(items[1]));
        expect(state).toEqual({
          hasErrors: false,
          items: [items[0], items[1]],
          dietCounts: { "df":2, "gf":2, "n!":1, "v":1, "ve":1, "rsf":1}
        });
      });
    });
  });

  describe('removing items', () => {

    describe('when an item is removed', () => {
      it('should have no items in the state with the correct diet count', () => {
        let state = previewReducer(undefined, actions.addItem(items[0]));
        state = previewReducer(state, actions.removeItem(0));
        expect(state).toEqual({
          hasErrors: false,
          items: [],
          dietCounts: {}
        });
      });
    });

    describe('when an item is removed from 3 items', () => {
      it('should have 2 items in the state with the correct diet count', () => {
        let state = previewReducer(undefined, actions.addItem(items[0]));
        state = previewReducer(state, actions.addItem(items[1]));
        state = previewReducer(state, actions.addItem(items[2]));
        state = previewReducer(state, actions.removeItem(1));
        expect(state).toEqual({
          hasErrors: false,
          items: [items[0], items[2]],
          dietCounts: { "v":2, "ve":2, "df":2, "gf":2, "n!":2}
        });
      });
    });
  });

});
