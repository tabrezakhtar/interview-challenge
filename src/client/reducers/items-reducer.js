import * as actions from '../actions/items-actions';

export const initialState = {
  loading: true,
  hasErrors: false
};

export default function itemsReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_ITEMS:
      return {...state, loading: true};
    case actions.GET_ITEMS_SUCCESS:
      return { 
        ...state,
        items: action.payload,
        loading: false,
        hasErrors: false
      };
    default:
      return state
  }
}