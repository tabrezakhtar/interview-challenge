import { combineReducers } from 'redux'

import itemsReducer from './items-reducer';
import previewReducer from './preview-reducer';

const rootReducer = combineReducers({
  items: itemsReducer,
  preview: previewReducer
})

export default rootReducer
