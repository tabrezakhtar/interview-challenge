import rootReducer from './index';

describe('when the reducers are combined', () => {
  it('should return multiple reducers', () => {
    expect(rootReducer.length).toEqual(2);
  });
});