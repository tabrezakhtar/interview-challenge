import itemsReducer from './items-reducer';
import * as actions from '../actions/items-actions';

describe('items reducer', () => {
  it('should return the initial state', () => {
    expect(itemsReducer(undefined, {})).toEqual({loading: true, hasErrors: false});
  });

  describe('when data is loading', () => {
    it('should handle GET_ITEMS', () => {
      expect(itemsReducer(undefined, actions.getItems())).toEqual({loading: true, hasErrors: false});
    });
  });

  describe('when data has loaded', () => {
    it('should handle GET_ITEMS_SUCCESS', () => {
      expect(itemsReducer(undefined, actions.getItemsSuccess({test:'payload'})))
        .toEqual({loading: false, hasErrors: false, items: {test:'payload'}});
    });
  });
});
