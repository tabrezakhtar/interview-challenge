import * as actions from '../actions/preview-actions';

export const initialState = {
  hasErrors: false,
  items: [],
  dietCounts: {}
};

function dietCounts(items) {
  let diets = {};

  for (const item of items) {
    for (const diet of item.dietaries) {
      if (diets[diet]) {
        diets[diet]++;
      } else {
        diets[diet] = 1;
      }
    }
  }

  return diets;
}

export default function previewReducer(state = initialState, action) {
  switch (action.type) {
    case actions.ADD_ITEM:
      return { 
        ...state,
        items: [...state.items, action.payload],
        dietCounts: dietCounts([...state.items, action.payload])
    }
    case actions.REMOVE_ITEM: {
      const removedItems = [
        ...state.items.slice(0, action.payload),
        ...state.items.slice(action.payload + 1)
      ];

      return { 
        ...state,
        items: removedItems,
        dietCounts: dietCounts(removedItems)
      };
    }
    default:
      return state
  }
}