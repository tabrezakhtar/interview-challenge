export const flattenDiets = (items) => {
  const diets = items.reduce( (prev, next) => prev.concat(next.dietaries), []);
  return [...new Set(diets)];
}