# Code test for Feedr
#### Completed by Tabrez Akhtar

<img src="https://gitlab.com/tabrezakhtar/interview-challenge/-/raw/master/screenshot.gif" alt="Image of Feedr test" width="600px"/>

To run the app:

`npm install`

`npm run dev`

The app will start on port 3000.

Open `localhost:3000`

Run unit tests:  
`npm test`

To see the code coverage:  
`npm run coverage`  
open `coverage/lcov-report/index.html` in the browser

## Improvements

- Search feature was not implemented

- Use redux hooks instead of connect

- Error states should be handled - e.g. data loading errors

- Better use of proptypes for default props and shape of object

- More test coverage